/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Transaction struct {
	Amount      string `json:"amount"`
	BlockNumber string `json:"block_number"`
	Hash        string `json:"hash"`
	ScAddress   string `json:"sc_address"`
	Timestamp   string `json:"timestamp"`
	Type        string `json:"type"`
	Wallet      string `json:"wallet"`
}
