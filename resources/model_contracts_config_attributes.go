/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ContractsConfigAttributes struct {
	Api       string   `json:"api"`
	Contracts []Config `json:"contracts"`
}
