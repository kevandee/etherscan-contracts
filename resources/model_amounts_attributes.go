/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type AmountsAttributes struct {
	Contracts    []Amount      `json:"contracts"`
	TotalAmount  string        `json:"totalAmount"`
	Transactions []Transaction `json:"transactions"`
}
