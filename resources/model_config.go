/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Config struct {
	Address  string `json:"address"`
	Duration string `json:"duration"`
}
