/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ResourceType string

// List of ResourceType
const (
	AMOUNT ResourceType = "contracts-amount"
	CONFIG ResourceType = "contracts-config"
)
