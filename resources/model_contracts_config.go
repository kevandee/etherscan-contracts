/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type ContractsConfig struct {
	Key
	Attributes ContractsConfigAttributes `json:"attributes"`
}
type ContractsConfigResponse struct {
	Data     ContractsConfig `json:"data"`
	Included Included        `json:"included"`
}

type ContractsConfigListResponse struct {
	Data     []ContractsConfig `json:"data"`
	Included Included          `json:"included"`
	Links    *Links            `json:"links"`
}

// MustContractsConfig - returns ContractsConfig from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustContractsConfig(key Key) *ContractsConfig {
	var contractsConfig ContractsConfig
	if c.tryFindEntry(key, &contractsConfig) {
		return &contractsConfig
	}
	return nil
}
