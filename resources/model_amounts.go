/*
 * GENERATED. Do not modify. Your changes might be overwritten!
 */

package resources

type Amounts struct {
	Key
	Attributes AmountsAttributes `json:"attributes"`
}
type AmountsResponse struct {
	Data     Amounts  `json:"data"`
	Included Included `json:"included"`
}

type AmountsListResponse struct {
	Data     []Amounts `json:"data"`
	Included Included  `json:"included"`
	Links    *Links    `json:"links"`
}

// MustAmounts - returns Amounts from include collection.
// if entry with specified key does not exist - returns nil
// if entry with specified key exists but type or ID mismatches - panics
func (c *Included) MustAmounts(key Key) *Amounts {
	var amounts Amounts
	if c.tryFindEntry(key, &amounts) {
		return &amounts
	}
	return nil
}
