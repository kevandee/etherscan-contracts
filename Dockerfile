FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/nft
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/etherscan-contracts /go/src/etherscan-contracts


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/nft /usr/local/bin/nft
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["nft"]
