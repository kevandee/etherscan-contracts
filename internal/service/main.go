package service

import (
	"context"
	"etherscan-contracts/internal/data/pg"
	"etherscan-contracts/internal/service/handlers"
	"net"
	"net/http"

	"etherscan-contracts/internal/config"
	"gitlab.com/distributed_lab/kit/copus/types"
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/distributed_lab/logan/v3/errors"
)

type service struct {
	log      *logan.Entry
	copus    types.Copus
	listener net.Listener
	cfg      config.Config
}

func (s *service) run() error {
	s.log.Info("Service started")
	ctx := context.Background()
	ctx = handlers.CtxLog(s.log)(ctx)
	ctx = handlers.CtxTransactionQ(pg.NewTransactionsQ(s.cfg.DB()))(ctx)
	ctx = handlers.CtxContracts(s.cfg.Contracts())(ctx)
	StartGoroutines(ctx)

	r := s.router()

	if err := s.copus.RegisterChi(r); err != nil {
		return errors.Wrap(err, "cop failed")
	}

	return http.Serve(s.listener, r)
}

func newService(cfg config.Config) *service {
	return &service{
		log:      cfg.Log(),
		copus:    cfg.Copus(),
		listener: cfg.Listener(),
		cfg:      cfg,
	}
}

func Run(cfg config.Config) {
	if err := newService(cfg).run(); err != nil {
		panic(err)
	}
}
