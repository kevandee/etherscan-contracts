package service

import (
	"etherscan-contracts/internal/data/pg"
	"etherscan-contracts/internal/service/handlers"
	"github.com/go-chi/chi"
	"gitlab.com/distributed_lab/ape"
)

func (s *service) router() chi.Router {
	r := chi.NewRouter()

	r.Use(
		ape.RecoverMiddleware(s.log),
		ape.LoganMiddleware(s.log),
		ape.CtxMiddleware(
			handlers.CtxLog(s.log),
			handlers.CtxContracts(s.cfg.Contracts()),
			handlers.CtxTransactionQ(pg.NewTransactionsQ(s.cfg.DB())),
		),
	)
	r.Route("/contracts", func(r chi.Router) {
		r.Get("/", handlers.GetContractTransactions)
		r.Get("/config", handlers.GetContractsConfig)
	})

	return r
}
