package service

import (
	"context"
	"etherscan-contracts/internal/service/handlers"
	"gitlab.com/distributed_lab/running"
	"strconv"
	"time"
)

func StartGoroutines(ctx context.Context) {
	contractsCfg := handlers.BackendContracts(ctx)

	for index, contract := range contractsCfg.Contracts {
		go running.WithBackOff(ctx, handlers.BackendLog(ctx),
			strconv.Itoa(index),
			handlers.NewGoroutineFunc(contract, contractsCfg),
			time.Duration(contract.Duration)*time.Millisecond,
			3*time.Second,
			3*time.Second,
		)
	}

}
