package handlers

import (
	"context"
	"etherscan-contracts/internal/config"
	"etherscan-contracts/internal/data"
	"net/http"

	"gitlab.com/distributed_lab/logan/v3"
)

type ctxKey int

const (
	logCtxKey ctxKey = iota
	transactionQCtxKey
	contractsCtxKey
)

func CtxLog(entry *logan.Entry) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, logCtxKey, entry)
	}
}

func Log(r *http.Request) *logan.Entry {
	return r.Context().Value(logCtxKey).(*logan.Entry)
}

func BackendLog(ctx context.Context) *logan.Entry {
	return ctx.Value(logCtxKey).(*logan.Entry)
}

func CtxTransactionQ(entry data.TransactionQ) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, transactionQCtxKey, entry)
	}
}

func TransactionQ(r *http.Request) data.TransactionQ {
	return r.Context().Value(transactionQCtxKey).(data.TransactionQ)
}

func BackendTransactionQ(ctx context.Context) data.TransactionQ {
	return ctx.Value(transactionQCtxKey).(data.TransactionQ).New()
}

func CtxContracts(entry config.Contract) func(context.Context) context.Context {
	return func(ctx context.Context) context.Context {
		return context.WithValue(ctx, contractsCtxKey, entry)
	}
}

func Contracts(r *http.Request) config.Contract {
	return r.Context().Value(contractsCtxKey).(config.Contract)
}

func BackendContracts(ctx context.Context) config.Contract {
	return ctx.Value(contractsCtxKey).(config.Contract)
}
