package handlers

import (
	"etherscan-contracts/internal/config"
	"etherscan-contracts/internal/types"
	"etherscan-contracts/resources"
	"gitlab.com/distributed_lab/ape"
	"net/http"
	"strconv"
)

func GetContractsConfig(w http.ResponseWriter, r *http.Request) {
	cfg := Contracts(r)
	result := resources.ContractsConfigResponse{
		Data: newContractsConfigModel(cfg),
	}

	ape.Render(w, result)
}

func newContractsConfigModel(cfg config.Contract) resources.ContractsConfig {
	result := resources.ContractsConfig{
		Key: resources.NewKeyInt64(1, resources.CONFIG),
		Attributes: resources.ContractsConfigAttributes{
			Api:       cfg.Api,
			Contracts: getContractsArray(cfg.Contracts),
		},
	}

	return result
}

func getContractsArray(contracts []types.Contract) []resources.Config {
	result := make([]resources.Config, 0, len(contracts))
	for _, contract := range contracts {
		result = append(result, resources.Config{
			Address:  contract.Address,
			Duration: strconv.FormatInt(contract.Duration, 10),
		})
	}
	return result
}
