package handlers

import (
	"context"
	"encoding/json"
	"etherscan-contracts/internal/config"
	"etherscan-contracts/internal/types"
	"fmt"
	"io"
	"net/http"
	"strings"
)

type statusCheck struct {
	Status string `json:"status"`
}

type apiResponse struct {
	Status  string              `json:"status"`
	Message string              `json:"message"`
	Result  []map[string]string `json:"result"`
}

func NewGoroutineFunc(contract types.Contract, cfg config.Contract) func(context.Context) error {
	return func(ctx context.Context) error {
		for page := int32(1); ; page++ {
			lastTx, err := BackendTransactionQ(ctx).GetMaxBlockNum(strings.ToLower(contract.Address))
			startBlock := lastTx.BlockNumber + 1
			if err != nil {
				startBlock = 0
			}
			url := apiTxlistUrl(cfg.Api, contract.Address, startBlock, page, 10, cfg.ApiKey)
			response, err := getApiResponse(url)
			if err != nil {
				return err
			}

			if response.Status != "1" {
				break
			}
			for _, tx := range response.Result {
				HandleTransaction(ctx, tx)
			}
		}

		return nil
	}
}

func getApiResponse(url string) (apiResponse, error) {
	var apiRes apiResponse

	res, err := http.Get(url)
	if err != nil {
		return apiRes, err
	}
	var statusCheck statusCheck
	bodyBB, _ := io.ReadAll(res.Body)
	err = json.Unmarshal(bodyBB, &statusCheck)
	if err != nil {
		return apiRes, err
	}
	if statusCheck.Status == "0" {
		return apiRes, err
	}
	err = json.Unmarshal(bodyBB, &apiRes)
	if err != nil {
		return apiRes, err
	}

	return apiRes, nil
}

func apiTxlistUrl(api string, address string, startBlock int64, page int32, offset int32, apiKey string) string {
	return fmt.Sprintf("%s/api?module=account&action=txlist&address=%s&startblock=%d&endblock=99999999&page=%d&offset=%d&sort=asc&apikey=%s",
		api, address, startBlock, page, offset, apiKey)
}
