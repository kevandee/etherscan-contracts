package handlers

import (
	"etherscan-contracts/internal/data"
	"etherscan-contracts/internal/service/requests"
	"etherscan-contracts/resources"
	"gitlab.com/distributed_lab/ape"
	"gitlab.com/distributed_lab/ape/problems"
	"math/big"
	"net/http"
	"strconv"
	"strings"
)

func GetContractTransactions(w http.ResponseWriter, r *http.Request) {
	request, err := requests.NewGetContractsTransactionsRequest(r)
	if err != nil {
		ape.RenderErr(w, problems.BadRequest(err)...)
		return
	}

	txQ := TransactionQ(r).New()
	txs, err := txQ.GetTransactionsByWallet(strings.ToLower(request.Address))

	if err != nil {
		Log(r).WithError(err).Error("failed to get transactions")
		ape.Render(w, problems.InternalError())
		return
	}

	if len(txs) == 0 {
		Log(r).WithError(err).Error("not found")
		ape.Render(w, problems.NotFound())
		return
	}

	response := resources.AmountsResponse{
		Data: newContractAmountModel(&txs),
	}

	ape.Render(w, response)
}

func newContractAmountModel(txs *[]data.Transaction) resources.Amounts {
	result := make([]resources.Transaction, len(*txs))
	for i, tx := range *txs {
		result[i] = newTransactionModel(tx)
	}
	amounts, totalAmount := getContractAmounts(*txs)
	return resources.Amounts{
		Key: resources.NewKeyInt64(1, resources.AMOUNT),
		Attributes: resources.AmountsAttributes{
			TotalAmount:  totalAmount.String(),
			Contracts:    amounts,
			Transactions: result,
		},
	}
}

func newTransactionModel(tx data.Transaction) resources.Transaction {
	return resources.Transaction{
		Wallet:      tx.Wallet,
		ScAddress:   tx.ScAddress,
		Amount:      tx.Amount,
		Type:        tx.Type,
		Hash:        tx.Hash,
		Timestamp:   tx.TimeStamp,
		BlockNumber: strconv.FormatInt(tx.BlockNumber, 10),
	}
}

func getContractAmounts(txs []data.Transaction) ([]resources.Amount, big.Int) {
	var resultArr []resources.Amount
	totalAmount := big.NewInt(0)
	for _, tx := range txs {
		index := -1
		var curAmount big.Int
		curAmount.SetString(tx.Amount, 10)
		totalAmount = totalAmount.Add(totalAmount, &curAmount)
		for i, amountItem := range resultArr {
			if tx.ScAddress == amountItem.Address {
				index = i
				break
			}
		}

		if index != -1 {
			var resAmount big.Int
			resAmount.SetString(resultArr[index].Amount, 10)
			resultArr[index].Amount = resAmount.Add(&resAmount, &curAmount).String()
			continue
		}

		resultArr = append(resultArr, resources.Amount{
			Address: tx.ScAddress,
			Amount:  tx.Amount,
		})
	}

	return resultArr, *totalAmount
}
