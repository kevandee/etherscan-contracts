package handlers

import (
	"context"
	"encoding/hex"
	"etherscan-contracts/internal/data"
	"log"
	"math/big"
	"strconv"
	"strings"
)

func HandleTransaction(ctx context.Context, tx map[string]string) {
	if tx["isError"] == "0" && tx["txreceipt_status"] == "1" {
		blockNumber, _ := strconv.Atoi(tx["blockNumber"])
		var typeTx string
		if strings.Contains(tx["functionName"], "stake") {
			typeTx = "stake"
		} else {
			return
		}

		amount := getAmount(tx["input"])
		BackendTransactionQ(ctx).Insert(data.Transaction{
			Type:        typeTx,
			Wallet:      tx["from"],
			ScAddress:   tx["to"],
			Amount:      amount.String(),
			TimeStamp:   tx["timeStamp"],
			Hash:        tx["hash"],
			BlockNumber: int64(blockNumber),
		})
	}
}

func getAmount(hexInput string) big.Int {
	decodedData, err := hex.DecodeString(hexInput[10:])
	if err != nil {
		log.Fatal(err)
	}
	var val big.Int
	val.SetBytes(decodedData)
	return val
}
