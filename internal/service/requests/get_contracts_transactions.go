package requests

import (
	"gitlab.com/distributed_lab/kit/pgdb"
	"gitlab.com/distributed_lab/urlval"
	"net/http"
)

type GetContractsTransactionsRequest struct {
	pgdb.OffsetPageParams
	Address string `url:"wallet"`
}

func NewGetContractsTransactionsRequest(r *http.Request) (GetContractsTransactionsRequest, error) {
	request := GetContractsTransactionsRequest{}

	err := urlval.Decode(r.URL.Query(), &request)
	if err != nil {
		return request, err
	}

	return request, nil
}
