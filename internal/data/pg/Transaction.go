package pg

import (
	"database/sql"
	"etherscan-contracts/internal/data"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/fatih/structs"
	"gitlab.com/distributed_lab/kit/pgdb"
)

const transactionsTableName = "transactions"

func NewTransactionsQ(db *pgdb.DB) data.TransactionQ {
	return &transactionsQ{
		db:  db.Clone(),
		sql: sq.Select("n.*").From(fmt.Sprintf("%s as n", transactionsTableName)),
	}
}

type transactionsQ struct {
	db  *pgdb.DB
	sql sq.SelectBuilder
}

func (q *transactionsQ) New() data.TransactionQ {
	return NewTransactionsQ(q.db)
}

func (q *transactionsQ) Get() (*data.Transaction, error) {
	var result data.Transaction
	err := q.db.Get(&result, q.sql)
	if err == sql.ErrNoRows {
		return nil, nil
	}

	return &result, err
}

func (q *transactionsQ) Select() ([]data.Transaction, error) {
	var result []data.Transaction
	err := q.db.Select(&result, q.sql)
	return result, err
}

func (q *transactionsQ) Transaction(fn func(q data.TransactionQ) error) error {
	return q.db.Transaction(func() error {
		return fn(q)
	})
}

func (q *transactionsQ) Insert(value data.Transaction) (data.Transaction, error) {
	clauses := structs.Map(value)
	clauses["wallet"] = value.Wallet
	clauses["sc_address"] = value.ScAddress
	clauses["amount"] = value.Amount
	clauses["type"] = value.Type
	clauses["hash"] = value.Hash
	clauses["timestamp"] = value.TimeStamp
	clauses["block_number"] = value.BlockNumber

	var result data.Transaction
	stmt := sq.Insert(transactionsTableName).SetMap(clauses).Suffix("returning *")
	err := q.db.Get(&result, stmt)

	return result, err
}

func (q *transactionsQ) Page(pageParams pgdb.OffsetPageParams) data.TransactionQ {
	q.sql = pageParams.ApplyTo(q.sql, "id")
	return q
}

func (q *transactionsQ) FilterByID(ids ...int64) data.TransactionQ {
	q.sql = q.sql.Where(sq.Eq{"n.id": ids})
	return q
}

func (q *transactionsQ) FilterByScAddress(address string) data.TransactionQ {
	q.sql = q.sql.Where(sq.Or{sq.Eq{"n.sc_address": address}})

	return q
}

func (q *transactionsQ) FilterByWallet(address string) data.TransactionQ {
	q.sql = q.sql.Where(sq.Or{sq.Eq{"n.wallet": address}})

	return q
}

func (q *transactionsQ) DeleteById(ids ...int64) (*data.Transaction, error) {
	tx, err := q.FilterByID(ids...).Get()
	if err != nil {
		return nil, err
	}
	sqlStatement := "DELETE FROM " + transactionsTableName + " WHERE id = $1;"

	err = q.db.ExecRaw(sqlStatement, ids[0])
	return tx, err
}

func (q *transactionsQ) GetMaxBlockNum(address string) (data.Transaction, error) {
	q.FilterByScAddress(address)
	q.sql = q.sql.OrderBy("block_number desc").Limit(1)
	result, err := q.Get()

	if err != nil || result == nil {
		return data.Transaction{}, err
	}

	return *result, err
}

func (q *transactionsQ) GetTransactionsByWallet(address string) ([]data.Transaction, error) {
	q.FilterByWallet(address)
	q.sql = q.sql.OrderBy("timestamp asc")

	result, err := q.Select()
	if err != nil {
		return result, err
	}

	return result, err
}
