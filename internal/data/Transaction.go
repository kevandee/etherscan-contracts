package data

import (
	"gitlab.com/distributed_lab/kit/pgdb"
)

type TransactionQ interface {
	New() TransactionQ

	Get() (*Transaction, error)
	Select() ([]Transaction, error)

	Transaction(fn func(q TransactionQ) error) error

	Insert(data Transaction) (Transaction, error)

	Page(pageParams pgdb.OffsetPageParams) TransactionQ

	GetMaxBlockNum(address string) (Transaction, error)
	GetTransactionsByWallet(address string) ([]Transaction, error)
	FilterByID(id ...int64) TransactionQ
	FilterByScAddress(address string) TransactionQ
	FilterByWallet(address string) TransactionQ
	DeleteById(id ...int64) (*Transaction, error)
}

type Transaction struct {
	ID          int64  `db:"id" structs:"-"`
	Wallet      string `db:"wallet" structs:"-"`
	ScAddress   string `db:"sc_address" structs:"-"`
	Type        string `db:"type" structs:"-"`
	Amount      string `db:"amount" structs:"-"`
	Hash        string `db:"hash" structs:"-"`
	TimeStamp   string `db:"timestamp" structs:"-"`
	BlockNumber int64  `db:"block_number" structs:"-"`
}
