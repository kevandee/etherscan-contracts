-- +migrate Up

CREATE TYPE enum AS ENUM ('stake', 'withdraw');

create table transactions (
   id bigserial primary key,
   wallet text not null,
   sc_address text not null,
   amount text not null,
   type enum default 'stake',
   hash text not null,
   timestamp bigint not null,
   block_number  bigint not null
);

-- +migrate Down

drop table if exists transactions;
drop type if exists enum;