package types

type Contract struct {
	Address  string `json:"address"`
	Duration int64
}
