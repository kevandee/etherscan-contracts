package config

import (
	"etherscan-contracts/internal/types"
	"gitlab.com/distributed_lab/kit/kv"
)

type Contract struct {
	Api       string
	ApiKey    string
	Contracts []types.Contract
}

func (c *ViperConfig) Contracts() Contract {
	contractsMap := kv.MustGetStringMap(c.getter, "contracts")
	var res Contract
	res.Api = contractsMap["api"].(string)
	res.ApiKey = contractsMap["api_key"].(string)
	res.Contracts = c.GetContracts()
	return res
}

func (c *ViperConfig) GetContracts() []types.Contract {
	contractsMap := kv.MustGetStringMap(c.getter, "contracts")
	arr := contractsMap["contracts"].([]interface{})
	res := make([]types.Contract, 0, len(arr))

	for _, item := range arr {
		val := item.(map[interface{}]interface{})
		res = append(res, types.Contract{
			Address:  val["address"].(string),
			Duration: int64(val["duration"].(int)),
		})
	}

	return res
}
