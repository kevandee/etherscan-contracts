package main

import (
	"os"

	"etherscan-contracts/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
